import { useContext, useState,useEffect  } from "react";
import { useNavigate } from 'react-router-dom'
import { Button, Col, Container, Form, Row,  } from "react-bootstrap"
import UserContext from "../UserContext";
import Swal from "sweetalert2";



export default function ChangePassword () {

    const {user,setUser} = useContext(UserContext)


    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    
    const [loaded, setLoaded] = useState(false)

    //allow us to gain access to methods that will allows us to redirect the user to different page after enrolling a course
    const history = useNavigate();

    const [profile, setProfile] = useState({})

    useEffect (() => {

        fetch('https://secret-reef-26536.herokuapp.com/users/getUserDetails',{
        method: "GET",
        headers : {
             Authorization : `Bearer ${localStorage.getItem("token")}`
        }
    })
        .then (res => res.json())
        .then(data => {
            console.log(data)

            setProfile({
                id : data._id,
                password : data.password,
            })
            setLoaded(true)

           
        })
    }, [])


        function updatePassword (e) {

            e.preventDefault ();
 
    
            if(( password1 !== '' && password2 !== '') && (password1 !== password2)){
             
                    Swal.fire({
                        title : "Password Not Match",
                        icon: "error",
                        text : "Please Check your Password"
                    })
                }
                else {
                    fetch('https://secret-reef-26536.herokuapp.com/users/changePassword',{
                        method : "PUT",
                        headers : {
                            Authorization : `Bearer ${localStorage.getItem("token")}`,
                            'Content-Type' : 'application/json'
                       },
                        body : JSON.stringify({
    
                            password : password1,
    
                        })
                        })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data);
    
                            if(data !== null && data!==undefined && data !== " ")
                            {
                                setPassword1('');
                                setPassword2('');

                                Swal.fire({
                                    title: 'Password successfully change',
                                    icon: 'success',
                                    text: 'Welcome!'
                                })
                                
                                history("/profile")
        
                            } else {
        
                                Swal.fire({
                                    title: 'Something went wrong',
                                    icon: 'error',
                                    text: 'Please try again'
                                })
                            }
        
                        })
                }
        }



        return (
            <section key={loaded}>
               <Container>
                   <Row className="py-5">
                       <h1 className="text-center text-white">Change Password</h1>
                       <Col className="mx-auto p-5 formContainer">
                       <Form onSubmit={e => updatePassword(e)}>

                        <Form.Group controlId="password1">
                        <Form.Label>New Password</Form.Label>
                        <Form.Control
                            type="password"
                            value={password1}
                            onChange={ e => setPassword1(e.target.value)}
                            required
                        />
                        </Form.Group>

                        <Form.Group controlId="password2">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control
                            type="password"
                            value={password2}
                            onChange={ e => setPassword2(e.target.value)}
                            required
                        />
                        </Form.Group>
                
                <Button variant="warning" type="submit" id="submitBtn" className="mt-3 mb-3" >Submit</Button>
                    
            </Form>
                       </Col>
                   </Row>
               </Container>
            </section>
        )
}