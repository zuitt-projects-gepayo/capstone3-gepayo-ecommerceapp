import {useContext, useEffect,} from "react";
import {Navbar, Nav, NavDropdown} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from "../UserContext";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'

export default function AppNavBar () {

    const {user, setUser} = useContext(UserContext);
    console.log(user)

    useEffect(() => {

        fetch('https://secret-reef-26536.herokuapp.com/users/getUserDetails',{
          method : "GET",
          headers : {
            Authorization : `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)
    
          if(typeof data._id !== 'undefined'){
    
            setUser ({
              id : data._id,
              isAdmin : data.isAdmin
            })
          }
          else{
            setUser ({
              id : null,
              isAdmin : null
            })
          }
        })
    
      },[])

  const element = <FontAwesomeIcon icon={faUser} />

    return (
        <Navbar bg="dark" expand="lg" variant="dark" className="px-5 sticky-top">
            <Navbar.Brand as={Link} to="/"><span id='brandtext'>IT</span>Solutions</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className='m-auto'>
               
                { 
                    (user.id !== null && user.isAdmin !== true) ?

                    <>
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/products">Products</Nav.Link>
                    <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
                    <Nav.Link as={Link} to="/Orders">Orders</Nav.Link>
                    <NavDropdown title={element} id="navbarScrollingDropdown">
                    <NavDropdown.Item  as={Link} to="/changePassword">Change Password</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/logout">Logout</NavDropdown.Item>
                    </NavDropdown>
                    </>
                    :
                    (user.isAdmin === true) ?
                    <>
                    <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
                    <Nav.Link as={Link} to="/addProduct">Add Product</Nav.Link>
                    <Nav.Link as={Link} to="/adminProduct">Products</Nav.Link>
                    <NavDropdown title={element} id="navbarScrollingDropdown">
                    <NavDropdown.Item  as={Link} to="/changePassword">Change Password</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/logout">Logout</NavDropdown.Item>
                    </NavDropdown>
                    </>
                    :
                    <>
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/products">Products</Nav.Link>
                    <Nav.Link as={Link} to="/register">Register</Nav.Link>
                    <Nav.Link as={Link} to="/login">Log In</Nav.Link>
                    </>
                }
            </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}