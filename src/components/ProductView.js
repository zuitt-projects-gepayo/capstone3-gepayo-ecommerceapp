import { useState, useEffect, useContext } from "react";
import { Button, Card, Container, Row, Col } from "react-bootstrap";
import { useParams,useNavigate,Link } from "react-router-dom";
//v5: useNavigate is useHistory
import UserContext from "../UserContext";
import Swal from "sweetalert2";


export default function ProductView () {


    const {user} = useContext(UserContext)

    //allow us to gain access to methods that will allows us to redirect the user to different page after enrolling a course
    const history = useNavigate();

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);


    const {productId} = useParams();


    const order = (productId) => {

        fetch ('https://secret-reef-26536.herokuapp.com/users/purchaseProduct',{
            method : "POST",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productId : productId,
                productName : name,
                productPrice : price,
                productDescription : description

            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

           if(data.stock !== 0){
               Swal.fire({
                   title: "Ordered Successfully",
                   icon: "success",
                   text :"You have Successfully ordered 1 Item"
               })

               history("/products")
               // v5: history.push("/endpoint")
           }
           
           else{
            Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text :"Please contact admin"
            })
           }

        })
    };

    const fetchData = (productId) =>{
        fetch (`https://secret-reef-26536.herokuapp.com/products/${productId}`)
        .then(res =>res.json())
        .then(data => {
            console.log(data)

            setName (data.name);
            setDescription(data.description);
            setPrice(data.price)
            setStock(data.stock)
        })
    }


    useEffect(() => {

        fetchData(productId)

    },[])

    return (
        <section>
        <Container className="pt-5">
            <Row>
                <Col lg={{span: 6, offset : 3}}>
                    <Card>
                        <Card.Body >
                            <Card.Title>{name}</Card.Title>

                            <Card.Subtitle>Description</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>

                            <Card.Subtitle>Price</Card.Subtitle>
                            <Card.Text>Php {price}</Card.Text>

                            <Card.Subtitle>Stocks</Card.Subtitle>
                            <Card.Text>{stock}</Card.Text>

                            {
                            (user.id !== null && user.isAdmin === false) ?

                                <Button variant="warning" onClick={() => order(productId)}>Order</Button>
                                :
                                (user.isAdmin === true) ?
                                <Button variant="warning" onClick={() => order(productId)} disabled>Order</Button>
                                :
                                <Button variant="danger" type="submit" disabled>Please Login in to Order</Button>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
        </section>
    )
};