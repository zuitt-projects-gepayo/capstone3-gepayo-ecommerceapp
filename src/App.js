import { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Login from './pages/Login';
import Profile from './pages/Profile';
import Logout from './pages/Logout';
import Home from './pages/Home';
import Register from './pages/Register';
import Product from './pages/Product';
import ProductView from './components/ProductView';
import Error from './pages/Error';
import './App.css';
import AppNavBar from './components/AppNavBar';
import AdminProducts from './pages/AdminProducts';
import { UserProvider } from './UserContext'
import UpdateProfile from './components/UpdateProfile';
import ChangePassword from './components/ChangePassword';
import UserOrder from './pages/UserOrder';
import AddProduct from './pages/AddProduct';
import UpdateProduct from './pages/UpdateProduct';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin : null
  })


  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(() => {

    fetch('https://secret-reef-26536.herokuapp.com/users/getUserDetails',{
      method : "GET",
      headers : {
        Authorization : `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(typeof data._id !== 'undefined'){

        setUser ({
          id : data._id,
          isAdmin : data.isAdmin
        })
      }
      else{
        setUser ({
          id : null,
          isAdmin : null
        })
      }
    })

  },[])

  
  return(
<UserProvider value={{user,setUser,unsetUser}}>
  <Router>
      <AppNavBar/>
        <Routes>
            <Route exact path= "/" element={<Home/>}/>
            <Route exact path= "/login" element={<Login/>}/>
            <Route exact path= "/register" element={<Register/>}/>
            <Route exact path= "/products" element={<Product/>}/>
            <Route exact path= "/addProduct" element={<AddProduct/>}/>
            <Route exact path ="/products/:productId" element={<ProductView/>} />
            <Route exact path= "/adminProduct" element={<AdminProducts/>}/>
            <Route exact path= "/profile" element={<Profile/>}/>
            <Route exact path= "/changePassword" element={<ChangePassword/>}/>
            <Route exact path= "/profile/:userId" element={<UpdateProfile/>}/>
            <Route exact path= "/products/updateProduct/:productId" element={<UpdateProduct/>}/>
            <Route exact path= "/Orders" element={<UserOrder/>}/>
            <Route exact path= "/logout" element={<Logout/>}/>
            <Route exact path= "*" element={<Error/>}/>
        </Routes>
  </Router>
  </UserProvider>
  )
}

export default App;
