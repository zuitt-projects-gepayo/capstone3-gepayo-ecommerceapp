import { useState, useEffect, useContext} from "react";
import { Navigate, useNavigate } from 'react-router-dom'
import { Col, Form, Row, Button } from "react-bootstrap";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register () {

    const {user} = useContext(UserContext);

    const history = useNavigate();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [address, setAddress] = useState('')
    const [isActive, setIsActive] = useState(false);


    function registerUser (e) {

        e.preventDefault ();

        fetch('https://secret-reef-26536.herokuapp.com/users/checkEmail',{
            method : 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
          
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true){
                Swal.fire({
                    title : "Email already Existed",
                    icon: "error",
                    text : "please provide another Email"
                })
            }
            else {
                fetch('https://secret-reef-26536.herokuapp.com/users/registration',{
                    method : "POST",
                    headers : {
                        'Content-Type' : 'application/json'
                    },
                    body : JSON.stringify({

                        firstName : firstName,
                        lastName : lastName,
                        mobileNo : mobileNo,
                        address : address,
                        email : email,
                        password : password
                    })
                    })
                    .then(res => res.json())
                    .then(data => {
                        console.log(data);

                        if(data === true)
                        {
                            setFirstName('');
                            setLastName('');
                            setMobileNo('');
                            setAddress('');
                            setEmail('');
                            setPassword('');

                            console.log("registered")

                            Swal.fire({
                                title: 'Registration successful',
                                icon: 'success',
                                text: 'Welcome!'
                            })
                            
                            


                            history("/login")
    
                        } else {
    
                            Swal.fire({
                                title: 'Something went wrong',
                                icon: 'error',
                                text: 'Please try again'
                            })
                        }
    
                    })
            }
        })

        //clear input fields
        setFirstName('');
        setLastName('');
        setMobileNo('');
        setAddress('');
        setEmail('');
        setPassword('');

    }

    
    useEffect (() => {

     
        if(firstName !== '' && lastName !== '' && mobileNo !== '' && email !== ' ' && password !== '' && address !== ''){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }

    }, [firstName, lastName, mobileNo, email, password, address])


















    return(
        <section className="pt-5">
            <h1 className="text-center text-white">Register</h1>
            <Row>
                <Col bg="light" xs={12} md="5" className="mx-auto formContainer p-3">
                <Form onSubmit={e => registerUser(e)}>

            <Form.Group controlId="firstName">
               <Form.Label>First Name</Form.Label>
               <Form.Control
                   type="text"
                   placeholder = "Input your First Name here"
                   value= {firstName}
                   onChange={ e => setFirstName(e.target.value)}
                   required
               />
            </Form.Group>

            <Form.Group controlId="lastName">
               <Form.Label>Last Name</Form.Label>
               <Form.Control
                   type="text"
                   placeholder = "Input your Last Name here"
                   value= {lastName}
                   onChange={ e => setLastName(e.target.value)}
                   required
               />
            </Form.Group>

            <Form.Group controlId="mobileNo">
               <Form.Label>Mobile Number</Form.Label>
               <Form.Control
                   type="text"
                   placeholder = "Mobile No here"
                   value= {mobileNo}
                   onChange={ e => setMobileNo(e.target.value)}
                   required
               />
            </Form.Group>

            <Form.Group controlId="address">
               <Form.Label>Address</Form.Label>
               <Form.Control
                   type="text"
                   placeholder = "Address"
                   value= {address}
                   onChange={ e => setAddress(e.target.value)}
                   required
               />
            </Form.Group>
               
           <Form.Group controlId="userEmail">
               <Form.Label>Email Address</Form.Label>
               <Form.Control
                   type="email"
                   placeholder = "Enter your Email Here"
                   value= {email}
                   onChange={ e => setEmail(e.target.value)}
                   required
               />
           </Form.Group>

           <Form.Group controlId="password">
               <Form.Label>Password</Form.Label>
               <Form.Control
                   type="password"
                   placeholder = "Enter your Password Here"
                   value= {password}
                   onChange={ e => setPassword(e.target.value)}
                   required
               />
           </Form.Group>
           
            {   
                isActive ?

                    <Button variant="warning" type="submit" id="submitBtn" className="mt-3 mb-3">Submit</Button>
                :
                    <Button variant="warning" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>Submit</Button>
            }
            
       </Form>
                </Col>
            </Row>
        </section>
    )
}