import { useEffect, useContext } from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
// v5: Redirect to

export default function Logout(){


	const {unsetUser, setUser} = useContext(UserContext);


	//clears the localStorage of the user's information
	unsetUser();
	// localStorage.clear()

	useEffect(() => {
		//set the user state back to its original value
		setUser ({
			id: null
		})
	},[])
	
	return(
		<Navigate to='/login'/>
	)
}