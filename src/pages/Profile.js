import { useContext, useEffect, useState } from "react";
import { Button, Col, Container, Row, Modal} from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";


export default function Profile () {

    const [profile, setProfile] = useState('')

        useEffect (() => {

            fetch('https://secret-reef-26536.herokuapp.com/users/getUserDetails',{
            method: "GET",
            headers : {
                 Authorization : `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then (res => res.json())
            .then(data => {
                console.log(data)
    
                setProfile({
                    id : data._id,
                    firstName : data.firstName,
                    lastName : data.lastName,
                    mobileNo : data.mobileNo,
                    address : data.address,
                    email : data.email
                })
    
               
            })
        }, [])



    
        return (
        <section>
            <Container>
                <Row className="py-5">
                <h2 className="text-center text-white">Profile</h2>
                    <Col className='p-5 justify-content formContainer'>
                    <p><strong>Name: </strong>{profile.firstName} {profile.lastName}</p>
                       <p><strong>Address: </strong> {profile.address}</p>
                       <p><strong>Mobile No: </strong>{profile.mobileNo}</p>
                       <p><strong>Email: </strong>{profile.email}</p>
                       <Button as={Link} to ={`/profile/${profile.id}`}>Update Profile</Button>
                    </Col>
                </Row>
            </Container>    
        </section>
    )
};