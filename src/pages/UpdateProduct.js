import { useContext, useState, useEffect } from "react";
import { useNavigate, useParams } from 'react-router-dom'
import { Button, Col, Container, Form, Row,  } from "react-bootstrap"
import UserContext from "../UserContext";
import Swal from "sweetalert2";



export default function UpdateProduct () {

    const history = useNavigate();

    const {user} = useContext(UserContext)

    const {productId} = useParams();



    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [stock, setStock] = useState('')
    const [category, setCategory] = useState('')
    const [specs, setSpecs] = useState({})

    const [loaded, setLoaded] = useState(false)



    const getProductDetails = (productId) => {

        fetch (`https://secret-reef-26536.herokuapp.com/products/${productId}`)
        .then(res =>res.json())
        .then(data => {
            setSpecs({
                
                name : data.name,
                description : data.description,
                price : data.price,
                stock : data.stock,
                category : data.category
            })
            setLoaded(true)
        })
    
    }

    const updateProduct = (e) => {
        console.log("inside updateProduct")
        e.preventDefault()

        
        fetch(`https://secret-reef-26536.herokuapp.com/products/updateProduct/${productId}`,{
                        method : "PUT",
                        headers : {
                            Authorization : `Bearer ${localStorage.getItem("token")}`,
                            'Content-Type' : 'application/json'
                       },
                        body : JSON.stringify({
                            name : name,
                            description : description,
                            price : price,
                            stock : stock,
                            category : category
                        })
                        })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data);

                            if(data === true)
                            {

                                Swal.fire({
                                    title: 'Update successful',
                                    icon: 'success',
                                    text: 'Welcome!'
                                })
                                
                                history("/adminProduct")
        
                            } else {
        
                                Swal.fire({
                                    title: 'Something went wrong',
                                    icon: 'error',
                                    text: 'Please try again'
                                })
                            }
        
                        })


    }

        useEffect(() => {
            getProductDetails(productId)
        },[productId])


    return (
        <section key={loaded}>
               <Container>
                   <Row className="py-5">
                       <h1 className="text-center text-white">Update Product</h1>
                       <Col className="mx-auto p-5 formContainer">
                       <Form onSubmit={e => updateProduct(e)}>

                        <Form.Group controlId="name">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder={specs.name}
                            value ={name}
                            onChange={ e => setName(e.target.value)}
                            required
                        />
                        </Form.Group>


                        <Form.Group controlId="category">
                        <Form.Label>Category</Form.Label>
                        <Form.Select 
                        value={category}
                        onChange={ e => setCategory(e.target.value)}
                        required>
                        <option>Case/Chasis</option>
                        <option>Ram</option>
                        <option>Processor</option>
                        <option>Power Supply</option>
                        <option>CPU Cooler / HeatSink</option>
                        <option>Case fans</option>
                        <option>Motherboard</option>
                        <option>Storage</option>
                        <option>Graphics Card</option>
                        <option>Chords</option>
                        <option>Monitor</option>
                        
                        <option>Others</option>
                        </Form.Select>
                        </Form.Group>

                    
                        <Form.Group controlId="price">
                        <Form.Label>Price</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder={specs.price}
                            value={price}
                            onChange={ e => setPrice(e.target.value)}
                            required
                        />
                        </Form.Group>

                        <Form.Group controlId="stock">
                        <Form.Label>Stock</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder={specs.stock}
                            value={stock}
                            onChange={ e => setStock(e.target.value)}
                            required
                        />
                        </Form.Group>

                        <Form.Group controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control
                            as="textarea"
                            placeholder={specs.description}
                            value={description}
                            onChange={ e => setDescription(e.target.value)}
                            required
                        />
                        </Form.Group>
                
                <Button variant="warning" type="submit" id="submitBtn" className="mt-3 mb-3" >Submit</Button>
                    
            </Form>
                       </Col>
                   </Row>
               </Container>
            </section>
    )
}