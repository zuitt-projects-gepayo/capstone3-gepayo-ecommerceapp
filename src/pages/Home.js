import { useEffect, useState } from "react";
import { Container, Carousel, Row, Col, Card } from "react-bootstrap";
import ProductCard from '../components/ProductCard';
import { Link } from "react-router-dom";



export default function Home () {

    const [products, setProducts] = useState();

    useEffect (() => {
        console.log("enter")
       fetch('https://secret-reef-26536.herokuapp.com/products/getAllActiveProducts')
       .then (res => res.json())
       .then (data => {
           setProducts(data.map(product => {
               return(
                       <ProductCard key={product._id} productProp={product} />
               );

       }));
       })
   }, [])





    return (
        
        <section className="homepage">
            <Container>
                <Row className="pt-5">
                <Carousel>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src="https://d30xqvs6b65d10.cloudfront.net/wp-content/uploads/2020/09/evga-rtx-30-series-cards-800x400.jpg"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src="https://lh6.googleusercontent.com/BSN0LraitTpWODm6m6mAL8uh3BVAWyXv5fYtCy9qeJhznowaW03HtKQqz1XduYZvs7_PFtCYjQmTu0PZ3mx04mo7GhPx6Nh1yfVjCwv9SDNhdhrPB-fTc_ZcFb2TBblXt3ZQ17lfBaiyOcXDtxM"
                    alt="Second slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src="https://d30xqvs6b65d10.cloudfront.net/wp-content/uploads/2021/04/mainpage-1-800x400.jpg"
                    alt="Third slide"
                    />
                </Carousel.Item>
                </Carousel>
                </Row>

                <Row className="mt-5">
                    <Col>
                    </Col>
                </Row>


                <h1 className="text-center header">PRODUCTS</h1>

                <Row>
                    {products}
                </Row>

                </Container>
        </section>
    )
}