import { useState, useContext, useEffect } from "react";
import { Button, Col, Container, Row, Table } from "react-bootstrap";
import { useNavigate ,Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";



export default function UserOrder () {

    const {user} = useContext(UserContext);

    const [orders, setOrders] = useState([]);
    const [name,setName] =useState('')
	const [description,setDescription] =useState('')
	const [price,setPrice] =useState(0)
	let [total,setTotal] =useState([''])
	const [stock,setStock] =useState('')


	const cancelOrder = (orderId) => {

        fetch (`https://secret-reef-26536.herokuapp.com/users/cancelOrder/${orderId}`,{
            method : "DELETE",
            headers : {
                Authorization : `Bearer ${localStorage.getItem("token")}`
            },
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

           if(data === true){
               Swal.fire({
                   title: "Ordered is Cancelled",
                   icon: "success",
                   text :"You have Successfully cancel an Order"
               })

			   window.location.reload()
               // v5: history.push("/endpoint")
           }
           else{
            Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text :"Please contact admin"
            })
           }

        })
		
    };



	const fetchUser = (e) => {
		fetch('https://secret-reef-26536.herokuapp.com/users/getUserOrders',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log("1")
			console.log(result)
			setOrders(result)			
		})
	}

	let TotalPrice = 0;
	for(let i=0; i<orders.length; i++)
	{
		TotalPrice = TotalPrice + orders[i].productPrice
	}
	console.log(TotalPrice)
	let token = localStorage.getItem('token')


	
	useEffect( () => {
	
		fetchUser()
		
		
	}, [])



    return (
        <section>
            <Container className="pt-5">
                    <h1 className="text-center text-white">Orders</h1>
                <Row>
                    <Col xs={12} className="pt-5">
					<Table striped bordered hover variant="light">
					<thead>
						<tr>
						<th>Product Name</th>
						<th>Product Description</th>
						<th>Price</th>
						<th>Action</th>
						</tr>
					</thead>

					<tbody>
						{
						orders.map(function(data) {
							return(
						<tr>
						<td>{data.productName}</td>
						<td>{data.productDescription}</td>
						<td>{data.productPrice}</td>
						<td>
							<Button variant="btn btn-danger" type="button" onClick={(e) => {cancelOrder(data._id)}} >Cancel</Button>
						</td>
						</tr>
						)
						})
						}

						<tr>
							<th><h2>TOTAL</h2></th>
							<th className="text-center"><h2>Php {TotalPrice}</h2></th>
							<th></th>
							<th></th>
						</tr>
						
			
					</tbody>
					</Table>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}