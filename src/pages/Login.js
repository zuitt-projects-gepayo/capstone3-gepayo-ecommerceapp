import { Button, Form, Row, Col } from "react-bootstrap";
import { useState, useEffect, useContext} from "react";
import { Navigate } from "react-router-dom";
import Swal from 'sweetalert2'
import UserContext from "../UserContext";

export default function Login (props) {

    const {user,setUser} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);


    function loginUser(e){
        e.preventDefault ();
            
        fetch('https://secret-reef-26536.herokuapp.com/users/loginUser', {
            method : "POST",
            headers : {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (typeof data.access !== "undefined"){
                localStorage.setItem('token',data.access)
                retrieveUserDetails(data.access)

                Swal.fire({
                    title : 'Login Successful',
                    icon : "success",
                    text : "Welcome!"
                })
            }
            else{
                Swal.fire({
                    title : 'Authentication Failed',
                    icon : "error",
                    text : "Please check you credentials"
                })
            }
        })

        setEmail('');
        setPassword('');


        const retrieveUserDetails = (token) => {
            fetch('https://secret-reef-26536.herokuapp.com/users/getUserDetails',{
                method: "GET",
                headers : {
                    Authorization : `Bearer ${token}`
                }
            })
            .then (res => res.json())
            .then(data => {
                // console.log(data._id)

                setUser ({
                    id: data._id,
                    isAdmin : data.isAdmin
                })
            })
        };

    }
    useEffect (() => {

        if(email !== '' && password!=='')
        {
            setIsActive(true)
        }
        else{
            setIsActive(false)
        }

    }, [email, password])

    return (
        (user.id !== null) ?
        <Navigate to="/"/>

       :
        <section>
        <Row className="py-5">
            <Col className="mx-auto p-5 formContainer" xs={12} md="5">
                <Form onSubmit={e => loginUser(e)}>
                <h1 className="text-center">Login</h1>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder = "Enter your Email Here"
                        value= {email}
                        onChange={ e => setEmail(e.target.value)}
                        required
                    />
                    
                </Form.Group>
                
                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder = "Input your password Here"
                        value= {password}
                        onChange={ e => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                {   
                        isActive ?

                            <Button variant="warning" type="submit" id="submitBtn" className="mt-3 mb-3 loginBtn">Login</Button>
                        :
                            <Button variant="warning" type="submit" id="submitBtn" className="mt-3 mb-3 loginBtn" disabled>Login</Button>
                    }
            </Form>
        </Col>
       </Row>
       </section>
    )
}