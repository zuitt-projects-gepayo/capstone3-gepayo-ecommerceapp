import { useState, useContext, useEffect } from "react";
import { Button, Col, Container, Row, Table } from "react-bootstrap";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import { Link } from 'react-router-dom'



export default function AdminProduct () {

    const {user} = useContext(UserContext);
    const [product, setProduct] = useState([]);


	const deleteProduct = (productId) => {

        fetch (`https://secret-reef-26536.herokuapp.com/products/delete/${productId}`,{
            method : "DELETE",
            headers : {
                Authorization : `Bearer ${localStorage.getItem("token")}`,
            },
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

           if(data === true){
               Swal.fire({
                   title: "Product is Deleted",
                   icon: "success",
                   text :"You have Successfully delete a Product"
               })

			   window.location.reload(false)
               // v5: history.push("/endpoint")
           }
           else{
            Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text :"Please contact admin"
            })
           }

        })
    };





	const ArchiveProduct = (productId) => {

        fetch (`https://secret-reef-26536.herokuapp.com/products/archive/${productId}`,{
            method : "PUT",
            headers : {
                Authorization : `Bearer ${localStorage.getItem("token")}`,
				'Content-Type' : 'application/json'
            },
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

           if(data === true){
               Swal.fire({
                   title: "Product is Archived",
                   icon: "success",
                   text :"You have Successfully Archived a Product"
               })

			   window.location.reload(false)
               // v5: history.push("/endpoint")
           }
           else{
            Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text :"Please contact admin"
            })
           }

        })
    };


	const ActivateProduct = (productId) => {

        fetch (`https://secret-reef-26536.herokuapp.com/products/activate/${productId}`,{
            method : "PUT",
            headers : {
                Authorization : `Bearer ${localStorage.getItem("token")}`,
				'Content-Type' : 'application/json'
            },
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

           if(data === true){
               Swal.fire({
                   title: "Product is Activated",
                   icon: "success",
                   text :"You have Successfully Activated a Product"
               })

			   window.location.reload(true)
               // v5: history.push("/endpoint")
           }
           else{
            Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text :"Please contact admin"
            })
           }

        })
    };

    const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://secret-reef-26536.herokuapp.com/products/getAllProduct',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			setProduct(result)

	
		})
	}
	console.log(user.isAdmin)
	useEffect( () => {
		fetchData()
	}, [])

	console.log(product)
	
    return (
        <section>
            <Container className="pt-5">
                    <h1 className="text-center text-white">Products</h1>
                <Row>
                    <Col xs={12} className="pt-5">
					<Table striped bordered hover variant="light">
					<thead>
						<tr>
						<th>Product Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Stocks</th>
                        <th>Category</th>
						<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{
						
						product.map(function(data) {
							return(
						<tr>
						<td>{data.name}</td>
						<td>{data.description}</td>
						<td>{data.price}</td>
						<td>{data.stock}</td>
                        <td>{data.category}</td>
						<td>
							<Button variant="btn btn-primary" as={Link} to ={`/products/updateProduct/${data._id}`}> Edit</Button>
							{
								(data.isActive === true) ?
								<Button variant="btn btn-info" onClick={() => {ArchiveProduct(data._id)}}>Archive</Button>
								:
								<Button variant="btn btn-success"onClick={() => {ActivateProduct(data._id)}}>Activate</Button>

							}
							
							<Button variant="btn btn-danger" onClick={() => {deleteProduct(data._id)}}>Delete</Button>
						</td>
						</tr>
							
						)
						})}
						
				
						
					</tbody>
					</Table>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}